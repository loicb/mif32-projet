#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COORDINATOR 0
#define BOOTSTRAPER 1


struct Area{
  int x1, x2, y1, y2; // Coordonnées d'un espace
  int rank; // rang du processus
} typedef Area;

struct Point{
  int x, y; //Coordinates of a point
  int rank; //Rank of the processus
} typedef Point;

struct Node{
  struct Node * next; // Liste chainée des tous les noeuds
  Area * area; //Zone qui correspond au noeud
} typedef Node;

struct Hash{
  Point * key; // Point.x + Point.y = key
  int value; // valeur du hash
} typedef Hash;

struct HashTable{
  Hash * list; // Liste des hash
} typedef HashTable;

int main(int argc, char ** argv){
  int size, rank;

  HashTable * table;
  FILE * f;
  char fileName[20];
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  srand(time(NULL));

  if(rank == 0){
    table = (HashTable *) malloc(sizeof(table));
    f = fopen(fileName, "a");
  }else{
    Point * p = (Point *) malloc(sizeof(Point));
    p->x = rand()%1000;
    p->y = rand()%1000;
    p->rank = rank;



  }

  if(f != NULL)
    fclose(f);
  MPI_Finalize();
}
